function autoFill(container, config, values) {
    const nameField = container.querySelector(config.fields.name);

    if (nameField) {
        nameField.value = values.name;
    }

    const numberField = container.querySelector(config.fields.number);

    if (numberField) {
        numberField.value = values.number;
    }

    const expiryField = container.querySelector(config.fields.expiry);

    if (expiryField) {
        expiryField.value = values.expiry;
    }

    const cvvField = container.querySelector(config.fields.cvv);

    if (cvvField) {
        cvvField.value = values.cvv;
    }
}

function getButton(onClick) {
    const node = document.createElement('button');

    node.innerText = 'Autofill';
    node.id = 'auto-fill-button';
    node.addEventListener('click', onClick);

    return node;
}

function render(container, config, values) {
    return () => {
        const buttonsContainer = document.querySelector(config.buttons);

        if (buttonsContainer && !buttonsContainer.querySelector('#auto-fill-button')) {
            const button = getButton(() => autoFill(container, config, values));

            buttonsContainer.appendChild(button);
        }
    };
}

function getContainerNode(config) {
    const result = document.querySelector(config.container);

    if (config.iframeContainer && result) {
        return result.contentDocument.querySelector(config.iframeContainer);
    }

    return result;
}

function setup({ config, values }) {
    const container = getContainerNode(config);
    const handleObserver = render(container, config, values);

    const observer = new MutationObserver(handleObserver);

    observer.observe(container, {
        attributes: true,
        childList: true,
        subtree: true,
    });

    handleObserver();
}

chrome.runtime.sendMessage({ message: 'SETUP' }, (response) => {
    if (!response.config) {
        return null;
    }

    return setup(response);
});
